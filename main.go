package main

import (
	"gitlab.com/rbs-nfsw/smga-core/server"
	"gitlab.com/rbs-nfsw/smga-core/server/xmpp"
	"os"
	"os/signal"
	"syscall"
	"gitlab.com/rbs-nfsw/smga-core/server/db"
	"gitlab.com/rbs-nfsw/smga-core/server/web"
	"gitlab.com/rbs-nfsw/smga-core/server/rcon"
)

func main() {
	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGTERM)
	signal.Notify(gracefulStop, syscall.SIGINT)

	config := server.GetConfigManager()
	logger := server.GetLogger()

	logger.Info("Starting server...")

	err := config.Load()

	if err != nil {
		logger.Error(err)
		os.Exit(1)
	}

	dbManager := db.GetDbManager()
	dbManager.SetDetails(
		config.FetchStringProperty("DB_USER"),
		config.FetchStringProperty("DB_PASSWORD"),
		config.FetchStringProperty("DB_NAME"),
	)

	err = dbManager.Init()

	if err != nil {
		logger.Error(err)
		os.Exit(1)
	}

	webServer := web.NewServer(1337)
	go webServer.Start()

	xmppServer := xmpp.NewServer(5333)

	rconServer := rcon.NewServer(1338, "abcdefghijklmnopqrstuvwxyz")
	go rconServer.Start()

	<-gracefulStop
	logger.Info("Shutting down...")
	xmppServer.Shutdown()
	webServer.Shutdown()
	rconServer.Shutdown()
	os.Exit(0)
}
