package web

import (
	"github.com/kataras/iris"
	"github.com/kataras/iris/context"
	"gitlab.com/rbs-nfsw/smga-core/server/data"
	"time"
)

func SystemInfo(context context.Context) {
	context.XML(data.SystemInfo{
		Branch:                 "production",
		ChangeList:             "123456",
		ClientVersion:          "637",
		ClientVersionCheck:     true,
		Deployed:               "8/20/2013 11:24:40",
		EntitlementsToDownload: true,
		ForcePermanentSession:  true,
		JidPrepender:           "nfsw",
		LauncherServiceUrl:     "http://10.100.15.202/LauncherService/onlineconfig.aspx",
		NucleusNamespace:       "nfsw-live",
		NucleusNamespaceWeb:    "nfs_web",
		PersonaCacheTimeout:    900,
		PortalDomain:           "soapboxrace.world",
		PortalSecureDomain:     "soapboxrace.world",
		PortalStoreFailurePage: "soapboxrace.world/webkit/pageLoadError",
		PortalTimeOut:          60000,
		ShardName:              "SBRW",
		Time:                   time.Now(),
		Version:                "1599",
	})
}

func CarClasses(context context.Context) {
	context.XML(data.ArrayOfCarClass{
		CarClasses: []data.CarClass{
			{CarClassHash: 0, MinRating: 1, MaxRating: 2},
		},
	})
}

func RootController() func(iris.Party) {
	return func(party iris.Party) {
		party.Get("systeminfo", SystemInfo)
		party.Get("carclasses", CarClasses)
	}
}
