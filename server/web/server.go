package web

import (
	stdContext "context"
	"fmt"
	"github.com/kataras/iris"
	"gitlab.com/rbs-nfsw/smga-core/server"
	"time"
	"github.com/kataras/iris/core/router"
)

type Server struct {
	Port uint
	App  *iris.Application
}

func NewServer(port uint) *Server {
	return &Server{
		Port: port,
	}
}

func (s *Server) Start() {
	s.App = iris.Default()
	s.App.Use(iris.Gzip)

	s.App.PartyFunc("/nfsw/Engine.svc", func(p router.Party) {
		p.PartyFunc("/", RootController())
	})

	s.App.Run(iris.Addr(fmt.Sprintf(":%d", s.Port)), iris.WithoutInterruptHandler)
}

func (s *Server) Shutdown() {
	server.GetLogger().Infof("Stopping web server...")

	timeout := 5 * time.Second
	ctx, _ := stdContext.WithTimeout(stdContext.Background(), timeout)
	s.App.Shutdown(ctx)
}
