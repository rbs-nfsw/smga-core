package rcon

import (
	"github.com/gorilla/websocket"
	"net/http"
	"fmt"
	"gitlab.com/rbs-nfsw/smga-core/server"
	"time"
	"context"
)

var wsUpgrade = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Server struct {
	AuthToken       string
	Port            uint
	HttpSrv         *http.Server
	ShutdownChannel chan bool
}

type Client struct {
	Authenticated bool
	RemoteAddress string
}

func NewServer(port uint, token string) *Server {
	return &Server{Port: port, AuthToken: token, ShutdownChannel: make(chan bool)}
}

func (s *Server) Start() {
	http.HandleFunc("/rcon", func(w http.ResponseWriter, r *http.Request) {
		conn, _ := wsUpgrade.Upgrade(w, r, nil) // error ignored for sake of simplicity
		client := Client{Authenticated:false, RemoteAddress:r.RemoteAddr}

		for {
			// Read message from browser
			_, msg, err := conn.ReadMessage()
			if err != nil {
				return
			}

			// Print the message to the console
			strMsg := string(msg)
			fmt.Printf("%s sent: %s\n", conn.RemoteAddr(), strMsg)

			if !client.Authenticated {
				if strMsg != s.AuthToken {
					conn.WriteMessage(websocket.TextMessage, []byte("Invalid authentication token"))
					conn.Close()
					return
				}
				client.Authenticated = true
				conn.WriteMessage(websocket.TextMessage, []byte("Authenticated"))
			} else {

			}
		}
	})

	srv := &http.Server{Addr: fmt.Sprintf(":%d", s.Port)}

	go func() {
		srv.ListenAndServe()
	}()

	s.HttpSrv = srv
	s.WaitShutdown()
}

func (s *Server) WaitShutdown() {
	<-s.ShutdownChannel
	server.GetLogger().Infof("Stopping RCON server...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//shutdown the server
	err := s.HttpSrv.Shutdown(ctx)
	if err != nil {
		server.GetLogger().Infof("Shutdown request error: %v", err)
	}
}

func (s *Server) Shutdown() {
	s.ShutdownChannel <- true
}