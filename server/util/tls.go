package util

import (
	"crypto/tls"
	"io/ioutil"
	"crypto/x509"
)

func NewTLSConfig(clientCertFile, clientKeyFile, caCertFile string) (*tls.Config, error) {
	tlsConfig := tls.Config{}

	cert, err := tls.LoadX509KeyPair(clientCertFile, clientKeyFile)

	if err != nil {
		return &tlsConfig, err
	}

	tlsConfig.Certificates = []tls.Certificate{cert}

	caCert, err := ioutil.ReadFile(caCertFile)

	if err != nil {
		return &tlsConfig, err
	}

	caCertPool := x509.NewCertPool()
	caCertPool.AppendCertsFromPEM(caCert)
	tlsConfig.RootCAs = caCertPool
	tlsConfig.BuildNameToCertificate()

	return &tlsConfig, nil
}
