package util

import "encoding/xml"

func MarshalToXML(data interface{}) string {
	m, e := xml.Marshal(data)

	if e != nil {
		panic(e)
	}

	return string(m)
}