package data

type ArrayOfProfileData struct {
	Profiles []UserProfileData `xml:"ProfileData"`
}

type UserProfileData struct {
	Boost             uint    `xml:"Boost"`
	Cash              uint    `xml:"Cash"`
	IconIndex         int     `xml:"IconIndex"`
	Level             int     `xml:"Level"`
	Motto             string  `xml:"Motto"`
	Name              string  `xml:"Name"`
	PercentToLevel    float32 `xml:"PercentToLevel"`
	PersonaId         int     `xml:"PersonaId"`
	Rating            float32 `xml:"Rating"`
	Rep               float32 `xml:"Rep"`
	RepAtCurrentLevel int     `xml:"RepAtCurrentLevel"`
}

type User struct {
	Address1                  string `xml:"address1"`
	Address2                  string `xml:"address2"`
	Country                   string `xml:"country"`
	DateCreated               string `xml:"dateCreated"`
	DateOfBirth               string `xml:"dob"`
	Email                     string `xml:"email"`
	EmailStatus               string `xml:"emailStatus"`
	FirstName                 string `xml:"firstName"`
	FullGameAccess            bool   `xml:"fullGameAccess"`
	Gender                    string `xml:"gender"`
	IdDigits                  string `xml:"idDigits"`
	IsComplete                bool   `xml:"isComplete"`
	LandlinePhone             string `xml:"landlinePhone"`
	Language                  string `xml:"language"`
	LastAuthDate              string `xml:"lastAuthDate"`
	LastName                  string `xml:"lastName"`
	Mobile                    string `xml:"mobile"`
	Nickname                  string `xml:"nickname"`
	PostalCode                string `xml:"postalCode"`
	RealName                  string `xml:"realName"`
	ReasonCode                string `xml:"reasonCode"`
	RemoteUserId              uint   `xml:"remoteUserId"`
	SecurityToken             string `xml:"securityToken"`
	StarterPackEntitlementTag string `xml:"starterPackEntitlementTag"`
	Status                    string `xml:"status"`
	SubscribeMsg              bool   `xml:"subscribeMsg"`
	TosVersion                string `xml:"tosVersion"`
	UserId                    uint   `xml:"userId"`
	Username                  string `xml:"username"`
}

type UserInfo struct {
	DefaultPersonaIdx int                `xml:"defaultPersonaIdx"`
	Personas          ArrayOfProfileData `xml:"personas"`
	User              User               `xml:"user"`
}
