package data

type ArrayOfCarClass struct {
	CarClasses []CarClass `xml:"CarClass"`
}

type CarClass struct {
	CarClassHash int   `xml:"CarClassHash"`
	MinRating    int16 `xml:"MinRating"`
	MaxRating    int16 `xml:"MaxRating"`
}
