package data

type ArrayOfBasketItemTrans struct {
	Items []BasketItemTrans `xml:"BasketItemTrans"`
}

type BasketItemTrans struct {
	ProductId string `xml:"ProductId"`
	Quantity  int    `xml:"Quantity"`
}

type BasketTrans struct {
	Items ArrayOfBasketItemTrans `xml:"Items"`
}

type CarSlotInfoTrans struct {

}

type CategoryTrans struct {

}

type ClientConfig struct {

}

type ClientConfigTrans struct {

}

type ClientLog struct {

}

type ClientServerCryptoTicket struct {

}

type CommerceItemTrans struct {

}

type CommerceResultStatus struct {

}

type CommerceResultTrans struct {

}

type CommerceSessionResultTrans struct {

}

type CommerceSessionTrans struct {

}

type Credentials struct {

}

type CustomCarTrans struct {

}

type CustomPaintTrans struct {

}

type CustomVinylTrans struct {

}