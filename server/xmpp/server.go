package xmpp

import (
	"net"
	"time"
	"sync"
	"github.com/olebedev/emitter"
	"bufio"
	"github.com/satori/go.uuid"
	"fmt"
	"strconv"
	"bytes"
	"crypto/tls"
	"gitlab.com/rbs-nfsw/smga-core/server/util"
	"gitlab.com/rbs-nfsw/smga-core/server"
)

// xmpp.Client is the class that represents a connected XMPP client.
// NFS:World uses JIDs in the format `PREFIX.PERSONAID`.
type Client struct {
	emitter.Emitter
	sync.RWMutex

	Connection     *net.Conn
	Address        *net.TCPAddr
	LastPacketTime time.Time
	JabberID       string
	Reader         *bufio.Reader
	Writer         *bufio.Writer
	IncomingBuffer []byte
	MessageQueue   chan string // pipeline of outgoing data
	HandshakeState int         // = stage - 1
}

// xmpp.Server is the class that manages the built-in XMPP server.
// The XMPP server is responsible for chat, whisper, groups, powerups, and more.
type Server struct {
	sync.Mutex
	Running        bool
	Port           uint
	Clients        sync.Map
	TLSConfig      *tls.Config
	MessageChannel chan struct{}
}

func NewServer(port uint) *Server {
	tlsConfig, _ := util.NewTLSConfig("xmpp-key.cer.pem", "xmpp-key.key.pem", "xmpp-key.cer.pem")

	tlsConfig.InsecureSkipVerify = true
	tlsConfig.ServerName = "192.168.6.13"

	xmppServer := Server{
		Port:           port,
		Clients:        sync.Map{},
		TLSConfig:      tlsConfig,
		Running:        true,
		MessageChannel: make(chan struct{}),
	}

	go xmppServer.Start()

	server.GetLogger().Infof("Started XMPP server on port %d", port)

	return &xmppServer
}

func (s *Server) Start() {
	addr, _ := net.ResolveTCPAddr("tcp", ":"+strconv.Itoa(int(s.Port)))
	listener, err := net.ListenTCP("tcp", addr)

	if err != nil {
		fmt.Println(err)
		return
	}

	defer func() {
		listener.Close()
		fmt.Println("Listener closed")
	}()

	for {
		select {
		case <-s.MessageChannel:
			break
		}

		// Get net.TCPConn object
		conn, err := listener.AcceptTCP()
		if err != nil {
			fmt.Println(err)
			break
		}

		client := s.NewClient(conn)

		fmt.Printf("New client connected - JID: %s IP: %s\n", client.JabberID, conn.RemoteAddr().String())

		go client.Listen()

		s.Clients.Store(client.Address.Port, client)
	}
}

func (s *Server) Shutdown() {
	server.GetLogger().Infof("Stopping XMPP server...")
	close(s.MessageChannel)
}

func (s *Server) RemoveDeadClient(client *Client) {
	fmt.Printf("Removing client [%s]\n", client.JabberID)
	s.Clients.Delete(client.Address.Port)
	(*client.Connection).Close()
}

func (s *Server) NewClient(connection net.Conn) *Client {
	jid := uuid.NewV4() // generate temp JID
	client := &Client{
		Connection:     &connection,
		JabberID:       jid.String(),
		IncomingBuffer: make([]byte, 2048),
		Address:        connection.RemoteAddr().(*net.TCPAddr),
		Reader:         bufio.NewReader(connection),
		Writer:         bufio.NewWriter(connection),
		MessageQueue:   make(chan string),
		HandshakeState: 0,
	}

	client.Emitter.Use("*", emitter.Void)

	client.On("data", func(event *emitter.Event) {
		s.HandleClientMessage(client, event.String(0))
	})

	client.On("disconnected", func(event *emitter.Event) {
		s.RemoveDeadClient(client)
	})

	return client
}

func (s *Server) HandleClientMessage(client *Client, message string) {
	switch client.HandshakeState {
	case 0: // stage 1 before SSL init
		client.Send("<stream:stream xmlns='jabber:client' xml:lang='en' xmlns:stream='http://etherx.jabber.org/streams' from='127.0.0.1' id='174159513' version='1.0'><stream:features><starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/></stream:features>")
		client.HandshakeState++
		break
	case 1: // stage 2 before SSL init
		client.Send("<proceed xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>")
		client.HandshakeState++

		defer client.TLSUpgrade(s.TLSConfig)

		break
	}
}

func (client *Client) ReadLoop() {
	defer func() {
		client.Emit("disconnected")
	}()

	for {
		_, err := client.Reader.Read(client.IncomingBuffer)

		if err != nil {
			panic(err)
		}

		go client.Emit("data", string(bytes.Trim(client.IncomingBuffer, "\x00")))
	}
}

func (client *Client) WriteLoop() {
	for data := range client.MessageQueue {
		client.Send(data)
	}
}

func (client *Client) Listen() {
	go client.ReadLoop()
	go client.WriteLoop()
}

func (client *Client) Send(data string) {
	client.Writer.WriteString(data)
	client.Writer.Flush()
}

func (client *Client) SendRaw(data []byte) {
	client.Writer.Write(data)
}

func (client *Client) TLSUpgrade(config *tls.Config) {
	conn := tls.Server(*client.Connection, config)

	conn.SetDeadline(time.Now().Add(5 * time.Second))
	err := conn.Handshake()

	if err != nil {
		fmt.Printf("ERROR: %s\n", err)
	} else {
		fmt.Println("SUCCESS")
	}
}
