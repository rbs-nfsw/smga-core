package db

import (
	"sync"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"fmt"
)

type Manager struct {
	Db         *gorm.DB
	DbUser     string
	DbPassword string
	DbName     string
}

var dbManager *Manager
var once sync.Once

func GetDbManager() *Manager {
	once.Do(func() {
		dbManager = &Manager{}
	})

	return dbManager
}

func (m *Manager) SetDetails(user string, password string, db string) {
	m.DbUser = user
	m.DbPassword = password
	m.DbName = db
}

func (m *Manager) Init() error {
	db, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@/%s?charset=utf8&parseTime=True&loc=Local", m.DbUser, m.DbPassword, m.DbName))

	if err != nil {
		return err
	}

	m.Db = db

	return nil
}
